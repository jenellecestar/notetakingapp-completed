//
//  ViewController.swift
//  NoteTakingApp
//
//  Created by robin on 2018-07-09.
//  Copyright © 2018 robin. All rights reserved.
//

import UIKit
import CoreData         // DON"T FORGET TO DO THIS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

class ViewController: UIViewController {

    // variables
    @IBOutlet weak var noteText: UITextField!
    
    var groceryList: [GroceryListItem] = []
    
    @IBOutlet weak var textView: UITextView!
    
    
    var managedContext : NSManagedObjectContext?    // making the variable global
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // create connection to the database
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        managedContext = appDelegate.persistentContainer.viewContext
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func addItem(_ sender: Any) {

        print("clicked!")
        
        // check if textbox is empty - LOGIC / UI NONSENSE -> VALIDATING THAT THE BOX HAS AN ITEM
        guard let x = noteText.text , x != "" else {
            print("textbox is empty")
            return
        }
        
        //======================================
        
        // 1. create a new CoreData GroceryItem object
        let groceryItemEntity = NSEntityDescription.entity(forEntityName: "GroceryItem", in: managedContext!)
        let item = NSManagedObject(entity: groceryItemEntity!, insertInto: managedContext!)
        
        // 2. set the properties of the CoreData GrocyerITem
        item.setValue(noteText.text!, forKey: "itemName")   // from text box
        item.setValue(Date(), forKey: "dateAdded")  // Date() -> automatically creates today's date
        
        // 3. Save the object to the database
        do {
            try managedContext!.save()
        }
        catch {
            print("Error saving to the database")
        }
        
        
        //======================================
        
        
        
        // clear the textbox - UI NONSENSE
        noteText.text = ""
        
        // create an alert box to show user
        
        // -- make an alert - UI NONSENE
        let abc = UIAlertController(title: "Save Complete", message: "Your item was saved!", preferredStyle: .alert)
        
        // -- add a button - UI NONSNSE
        abc.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))

        // -- show the popup - UI NONSNES
        // self.present(_______, animated: true, completion: nil)
        self.present(abc, animated: true, completion: nil)
       
        
    }

    @IBAction func showAll(_ sender: Any) {
        
        // 1. make a 'fetch request'
        //   - telling ios which table you want  ("GroceryItem")
        //   - tell ios what sql query you want  (SELECT *) by default
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "GroceryItem")
        
        // 3. send query to database
        do {
            let rows = try managedContext!.fetch(fetchRequest)
            for item in rows {
                let x = item as! GroceryItem
                print("\(x.dateAdded!): \(x.itemName!)")
            }
            
        }
        catch{
            print("Error fetching data from database")
        }
        
        // 4. process & display results
        
       
        
        
        
        
    }
}

